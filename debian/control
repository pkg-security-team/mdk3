Source: mdk3
Section: net
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Samuel Henrique <samueloph@debian.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/aircrack-ng/mdk3
Vcs-Browser: https://salsa.debian.org/pkg-security-team/mdk3
Vcs-Git: https://salsa.debian.org/pkg-security-team/mdk3.git

Package: mdk3
Architecture: linux-any
Depends: aircrack-ng,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Wireless attack tool for IEEE 802.11 networks
 MDK is a proof-of-concept tool to exploit common
 IEEE 802.11 (Wi-Fi) protocol weaknesses.
 Features:
   * Bruteforce MAC Filters.
   * Bruteforce hidden SSIDs (some small SSID wordlists included).
   * Probe networks to check if they can hear you.
   * Intelligent Authentication-DoS to freeze APs (with success checks).
   * FakeAP - Beacon Flooding with channel hopping (can crash NetStumbler and
     some buggy drivers)
   * Disconnect everything (aka AMOK-MODE) with Deauthentication and
     Disassociation packets.
   * WPA TKIP Denial-of-Service.
   * WDS Confusion - Shuts down large scale multi-AP installations.
